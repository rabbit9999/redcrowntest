<?php
require_once 'lib/db.php';
require_once 'lib/config.php';
require_once 'lib/helper.php';

error_reporting( E_ERROR );
ini_set('display_errors', 'Off');

$db = new Db();
$result = $db->getRandomRecord();

if(!$result){
    $db->fillData(Helper::readCsvFile());
    $result = $db->getRandomRecord();
}

if($result){
    unset($result['id']);
    Helper::printCsvLine($result);
}
else{
    echo "Wrong result...";
}

