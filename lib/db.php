<?php
class Db extends mysqli{

    private $dbHost;
    private $dbUser;
    private $dbPassword;
    private $dbName;

    function __construct(){

        $this->dbHost=Config::$dbHost;
        $this->dbUser=Config::$dbUser;
        $this->dbPassword=Config::$dbPassword;
        $this->dbName=Config::$dbName;

        parent::__construct($this->dbHost, $this->dbUser, $this->dbPassword, $this->dbName);


        if($this->connect_errno){
            $this->mysql_die();
        }

        $this->set_charset("utf8");
    }

    public function getRandomRecord(){
        //Это самый простой способ получить случайную запись, но он медленно работает на больших таблицах с миллионами записей
        $result=$this->query('SELECT `id`, `name`, `status` FROM `'.Config::$dbTable.'` ORDER BY rand() LIMIT 1');
        if(!$result){
            if($this->errno == 1146){ //Таблица отсутствует
                echo "Table doesn't exists...\n";
                return false;
            }
            else{
                $this->mysql_die();
            }
        }

        if($result->num_rows == 0){
            die("No result. Maybe table is empty?\n\n");
        }

        $assoc_result = $result->fetch_assoc();
        $result=$this->query('UPDATE `'.Config::$dbTable.'` SET `status` = NOT `status` WHERE `id`='.$assoc_result['id']);

        if(!$result){
            echo 'Failed to swap status: '.$this->error."\n";
        }

        return $assoc_result;
    }

    public function fillData($data){
        $this->createTable();

        if ($stmt = $this->prepare('INSERT INTO `'.Config::$dbTable.'`(`name`, `status`) VALUES(?,?)')) {

            echo "Filling data: ";
            foreach($data as $record){
                $stmt->bind_param("si", $record[0],$record[1]);
                $stmt->execute();
            }
            $stmt->close();
            echo "done\n";
        }
        else{
            $this->mysql_die();
        }

    }

    private function createTable(){
        $create_table_query = 'CREATE TABLE `'.Config::$dbTable.'` (
                `id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                `name` VARCHAR(30) NOT NULL,
                `status` BIT(1) NOT NULL
                ) DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci';

        $result = $this->query($create_table_query);

        if(!$result){
            $this->mysql_die();
        }
    }

    private function mysql_die(){
        die('mysql_error: '.$this->error."\n\n");
    }
}