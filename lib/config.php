<?php
abstract class Config{

    public static $csvFileName = 'data.csv';
    public static $csvDelimiter = ';';

    public static $dbName = 'redcrowntest';
    public static $dbHost = '127.0.0.1';
    public static $dbUser = 'root';
    public static $dbPassword = '123456';
    public static $dbTable = 'test_table';

}