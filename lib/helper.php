<?php
abstract class Helper{

    public static function readCsvFile(){
        $row = 0;
        $result = [];
        if (($file = fopen(Config::$csvFileName, "r")) !== FALSE) {
            while (($data = fgetcsv($file, 1000, Config::$csvDelimiter)) !== FALSE) {
                //Пропуск первой строки
                if($row == 0){
                    $row++;
                    continue;
                }
                $result[] = $data;
            }
            fclose($file);
        }
        else{
            die('csv_error: can`t open file `'.Config::$csvFileName."`\n");
        }
        return $result;
    }

    public static function printCsvLine($data){
        $out = fopen('php://output', 'w');
        fputcsv($out, $data, Config::$csvDelimiter);
        fclose($out);
    }

}
